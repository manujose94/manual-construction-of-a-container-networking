root@manu-GS60-6QE:/home/manu/Documents/MASTER/CC/dockerveth-master# ip netns exec ns_con2 ping -c 3 192.168.122.11
PING 192.168.122.11 (192.168.122.11) 56(84) bytes of data.
^C
--- 192.168.122.11 ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 2033ms

root@manu-GS60-6QE:/home/manu/Documents/MASTER/CC/dockerveth-master# ip netns exec ns_con2 arp -n
Address                  HWtype  HWaddress           Flags Mask            Iface
192.168.122.11           ether   4a:05:3d:44:1e:6a   C                     veth20
root@manu-GS60-6QE:/home/manu/Documents/MASTER/CC/dockerveth-master# iptables -A FORWARD -p icmp -j ACCEPT
root@manu-GS60-6QE:/home/manu/Documents/MASTER/CC/dockerveth-master# ip netns exec ns_con1 ping -c4 192.168.122.22
PING 192.168.122.22 (192.168.122.22) 56(84) bytes of data.
64 bytes from 192.168.122.22: icmp_seq=1 ttl=64 time=0.103 ms
64 bytes from 192.168.122.22: icmp_seq=2 ttl=64 time=0.077 ms
64 bytes from 192.168.122.22: icmp_seq=3 ttl=64 time=0.080 ms
^C
--- 192.168.122.22 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2048ms
rtt min/avg/max/mdev = 0.077/0.086/0.103/0.011 ms
root@manu-GS60-6QE:/home/manu/Documents/MASTER/CC/dockerveth-master# ip netns exec ns_con2 arp -n
Address                  HWtype  HWaddress           Flags Mask            Iface
192.168.122.11           ether   4a:05:3d:44:1e:6a   C                     veth20

