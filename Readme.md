# Manual construction of a container networking

How create our own Docker networking? The containers on Linux use network **namespaces** to established a isolation network. Thanks to use of techniques based on the network packet filter by the use of **IPTABLES** or newer **NFTABLES** (*netfilter*).

The main objective is to know how the communication between systems works in a deeper way, in order to solve future problems.

In case of doubt, please take a look at this section first: [here](#things-to-keep-in-mind)



## Exercises

Next is detailed 4 exercises where each one is an extension of the previous one, meaning the exercise 2 is an extension of the exercise 3 and so on.

The image used is based on Ubuntu 20 generated via `docker build` and  launched with a script named launch. Sh. The Dockerfile and `launch.sh`, both are located inside **docker-ubuntu20** folder.

> Note: All script must be lunched inside of `/scripts` folder
>

#### 1. Allow communication between two containers.

 ![](https://gitlab.com/manujose94/manual-construction-of-a-container-networking/-/raw/master/imgs/1-ejer.png)

> Note: It's necessary to launch script as root user
>

```
root:/scripts/# ./1-docker_point2point.sh
```

When the configuration has been implemented, then the script will finish with a small test.

Example of output generated due to small test:

```
Testing ping (con1)[veth0_cont1]----->[veth1_cont2](con2)
PING 192.168.122.2 (192.168.122.2) 56(84) bytes of data.
64 bytes from 192.168.122.2: icmp_seq=1 ttl=64 time=0.056 ms
64 bytes from 192.168.122.2: icmp_seq=2 ttl=64 time=0.082 ms

--- 192.168.122.2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3071ms
rtt min/avg/max/mdev = 0.056/0.079/0.093/0.014 ms

Testing ping (con2)[veth1_cont2]----->[veth0_cont1](con1)
PING 192.168.122.1 (192.168.122.1) 56(84) bytes of data.
64 bytes from 192.168.122.1: icmp_seq=1 ttl=64 time=0.025 ms
64 bytes from 192.168.122.1: icmp_seq=2 ttl=64 time=0.082 ms
```



#### 2. Containers can communicate with the host and allow communicating with containers from the host.

![](https://gitlab.com/manujose94/manual-construction-of-a-container-networking/-/raw/master/imgs/2a-ejer.png)

```
root:/scripts/# ./2-docker_bridge.sh

# Test

root:/scripts/# ./tests/2test.sh
```

An example of success output of test:

```
Testing... 

[1] Ping [container1] to [container2]

PING 192.168.122.22 (192.168.122.22) 56(84) bytes of data.
64 bytes from 192.168.122.22: icmp_seq=1 ttl=64 time=0.295 ms
64 bytes from 192.168.122.22: icmp_seq=2 ttl=64 time=0.137 ms

--- 192.168.122.22 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1032ms
rtt min/avg/max/mdev = 0.137/0.216/0.295/0.079 ms

[2] Ping [container2] to [container1]

PING 192.168.122.11 (192.168.122.11) 56(84) bytes of data.
64 bytes from 192.168.122.11: icmp_seq=1 ttl=64 time=0.044 ms
64 bytes from 192.168.122.11: icmp_seq=2 ttl=64 time=0.168 ms

--- 192.168.122.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1016ms
rtt min/avg/max/mdev = 0.044/0.106/0.168/0.062 ms

[3] Ping [container1] to [host]

PING 192.168.122.1 (192.168.122.1) 56(84) bytes of data.
64 bytes from 192.168.122.1: icmp_seq=1 ttl=64 time=0.062 ms
64 bytes from 192.168.122.1: icmp_seq=2 ttl=64 time=0.127 ms

--- 192.168.122.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1017ms

[4] Ping [host] to [container1]

PING 192.168.122.1 (192.168.122.1) 56(84) bytes of data.
64 bytes from 192.168.122.1: icmp_seq=1 ttl=64 time=0.078 ms
64 bytes from 192.168.122.1: icmp_seq=2 ttl=64 time=0.094 ms

--- 192.168.122.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1018ms
```



#### 3. Containers can communicate with any node reachable from the host

This exercise wants to get communication between containers and these can also communicate through the network.

<img src="https://gitlab.com/manujose94/manual-construction-of-a-container-networking/-/raw/master/imgs/3-ejer.png" style="zoom:80%;" />

```
root:/scripts/# ./3-fordwarding.sh

# Test

root:/scripts/# ./tests/3test.sh
```
Output success Test: 

```
Testing...

[1] Ping [container1] to [container2]

PING 192.168.122.22 (192.168.122.22) 56(84) bytes of data.
64 bytes from 192.168.122.22: icmp_seq=1 ttl=64 time=0.105 ms

--- 192.168.122.22 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms

[2] Ping [container2] to [container1]

PING 192.168.122.11 (192.168.122.11) 56(84) bytes of data.
64 bytes from 192.168.122.11: icmp_seq=1 ttl=64 time=0.029 ms

--- 192.168.122.11 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms

[3] Ping [container1] to [host]

PING 192.168.122.1 (192.168.122.1) 56(84) bytes of data.
64 bytes from 192.168.122.1: icmp_seq=1 ttl=64 time=0.062 ms

--- 192.168.122.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms

[4] Ping [host] to [container1]

PING 192.168.122.1 (192.168.122.1) 56(84) bytes of data.
64 bytes from 192.168.122.1: icmp_seq=1 ttl=64 time=0.030 ms
64 bytes from 192.168.122.1: icmp_seq=2 ttl=64 time=0.110 ms

 __External Nodes__ 
[5] Ping [container1] to DNS Server [8.8.8.8] (External host Internet)

PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=118 time=10.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=118 time=11.0 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms

[6] Ping [container1] to external hostname [gitlab.com] (Name resolution with DNS 8.8.8.8)

ping: gitlab.com: Nombre o servicio desconocido
	[Note] Maybe the systemd-resolved service is failing
	Check with: systemctl status systemd-resolved.service
```



#### 4. Containers mounted in different nodes can communicate with each other

<img src="https://gitlab.com/manujose94/manual-construction-of-a-container-networking/-/raw/master/imgs/4-ejer.png" style="zoom:80%;" />

The following script must be launched in both nodes:

`4-create-network-nodo.sh -n <<Nodo number>> -h <IP EXTERNAL HOST>`

This script is currently limited because only can select between two nodes (1,2), depending of node that we will have selected the script will generate a subnet (*Subnet Nodo 1 192.168.122.0/24 and subnet Nodo 2 192.168.133.0/24*). It's possible to change these networks, but it has been kept that way to verify via a test.

Note:  mandatory params **-n** and **-h**

**Nodo 2:**

> Params of the script: 4-create-network-nodo.sh
>
>  -n :  Indicate what is the current node that we want to implement [1 or 2]
>
>  -h: IP Host of the external PC that we want communicate
>
> Example nodo2 with IP Host: 192.168.1.97

```
root:/scripts/# ./4-create-network-nodo.sh -n 2 -h 192.168.1.108

```
**Nodo 1:** 

> Example nodo1 with IP Host: 192.168.1.108

```
root:/scripts/# ./4-create-network-nodo.sh -n 1 -h 192.168.1.97

# Test

root:/scripts/# ./tests/4test.sh
```

#### Future feature or expansion

##### Automate discovery process of containers

If we take a look to **section 4** above, we will see that the current implementation is of little use because we must to specify an IP host. So the next step will be that container will able to discover available containers atomically. There is an option using catching network events via **NETLINK**.

The following link more information: [DEEP DIVE 3 INTO DOCKER OVERLAY NETWORKS](https://blog.revolve.team/2017/08/20/deep-dive-3-into-docker-overlay-networks-part-3/)



### Things to keep in mind

- Any modification made via these script will be lost when PC has been rebooted


- **Iptables Firewall:** Any modification made using these commands is lost when you reboot the PC.

#### Image Docker Ubuntu 20:04

It is necessary before testing with the scripts, not to have any container with the name ubuntu20:veth.

The scripts will automatically remove such a container if it exists, and recreate it for testing.

Launch containers:

```
./docker-ubuntu20/launch.sh 

```

Stop and delete containers:

```
./docker-ubuntu20/launch.sh stop

```

#### Test of internal network Script

`./setinternal-network.sh 192.168.133`

`./setinternal-network.sh <<default 192.168.122>>`

#### Error DNS server via resolv.conf

If receive the message: "*Maybe the systemd-resolved service is failing*" from **4test.sh** or **3test.sh** then follow this next steps:

```shell
:~$ ls -ld /etc/resolv.conf
lrwxrwxrwx 1 root root 39 abr 27  2020 /etc/resolv.conf -> ../run/systemd/resolve/stub-resolv.conf
:~$ systemctl status systemd-resolved.service
● systemd-resolved.service - Network Name Resolution
     Loaded: loaded (/lib/systemd/system/systemd-resolved.service; enabled; vendor p>
     Active: active (running) since Thu 2020-10-28 18:16:57 CET; 3h 34min ago
       Docs: man:systemd-resolved.service(8)
             https://www.freedesktop.org/wiki/Software/systemd/resolved
             https://www.freedesktop.org/wiki/Software/systemd/writing-network-confi>
             https://www.freedesktop.org/wiki/Software/systemd/writing-resolver-clie>
   Main PID: 648 (systemd-resolve)
     Status: "Processing requests..."
      Tasks: 1 (limit: 18972)
     Memory: 10.5M
     CGroup: /system.slice/systemd-resolved.service
             └─648 /lib/systemd/systemd-resolved

oct 28 21:35:35 manu-GS60-6QE systemd-resolved[648]: Server returned error NXDOMAIN,>
oct 28 21:39:26 manu-GS60-6QE systemd-resolved[648]: Server returned error NXDOMAIN,>
oct 28 21:40:34 manu-GS60-6QE systemd-resolved[648]: Server returned error NXDOMAIN,>
```

This message error is exactly:
`Server returned error NXDOMAIN, mitigating potential DNS violation DVE-2018-0001, retrying transaction with reduced feature level UDP.`

The cause is the /etc/resolv.conf file which is linked to the wrong source file.

```
:~$ ls -ld /etc/resolv.conf
lrwxrwxrwx 1 root root 39 abr 27  2020 /etc/resolv.conf -> ../run/systemd/resolve/stub-resolv.conf
```

The Solution is remove the link in /etc and set it again in the source file `/var/run/systemd/resolve/resolv.conf`.

```
:~$ sudo rm /etc/resolv.conf
:~$ sudo ln -s /var/run/systemd/resolve/resolv.conf /etc/resolv.conf
:~$  ls -ld /etc/resolv.conf
lrwxrwxrwx 1 root root 36 oct 28 21:52 /etc/resolv.conf -> /var/run/systemd/resolve/resolv.conf
:~$ systemctl restart systemd-resolved.service

systemctl status systemd-resolved.service
● systemd-resolved.service - Network Name Resolution
     Loaded: loaded (/lib/systemd/system/systemd-resolved.service; enabled; vendor p>
     Active: active (running) since Thu 2020-10-28 21:53:02 CET; 4min 18s ago
       Docs: man:systemd-resolved.service(8)
             https://www.freedesktop.org/wiki/Software/systemd/resolved
             https://www.freedesktop.org/wiki/Software/systemd/writing-network-confi>
             https://www.freedesktop.org/wiki/Software/systemd/writing-resolver-clie>
   Main PID: 29448 (systemd-resolve)
     Status: "Processing requests..."
      Tasks: 1 (limit: 18972)
     Memory: 5.0M
     CGroup: /system.slice/systemd-resolved.service
             └─29448 /lib/systemd/systemd-resolved

oct 28 21:53:02 manu-GS60-6QE systemd[1]: Starting Network Name Resolution...
oct 28 21:53:02 manu-GS60-6QE systemd-resolved[29448]: Positive Trust Anchors:
oct 28 21:53:02 manu-GS60-6QE systemd-resolved[29448]: . IN DS 20326 8 2 e06d44b80b8>
oct 28 21:53:02 manu-GS60-6QE systemd-resolved[29448]: Negative trust anchors: 10.in>
oct 28 21:53:02 manu-GS60-6QE systemd-resolved[29448]: Using system hostname 'manu-G>
oct 28 21:53:02 manu-GS60-6QE systemd[1]: Started Network Name Resolution.
```

