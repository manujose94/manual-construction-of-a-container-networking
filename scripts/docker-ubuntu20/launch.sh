#!/bin/bash

NAME_IMAGE=manu/ubuntu20:veth
NAME_CONTAINER=con
TAG=manu/ubuntu20:veth
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

checkerror() {
	RESULT=$1
	if [ $RESULT != 0 ];then
		echo "[ERROR] Errors occured while launching image dockers"
		exit 127
	fi
}

# $1 first param stop allow
runContainer(){
	docker run  --rm -t -d \
	--net=none \
	--privileged \
	--name=$NAME_CONTAINER$1 $NAME_IMAGE bash
	checkerror $?
}



#matching=$(docker ps -a --filter="name=$NAME_IMAGE" -q | xargs)

if [[ $1 != *stop* && "$(docker images -q $TAG 2> /dev/null)" == "" ]]; then
	docker build --tag $TAG $__dir/.
	checkerror $?
fi
	
for i in 1 2
do
	CONTAINER=${NAME_CONTAINER}${i}
	matchingStarted=$(docker ps --filter="name=$CONTAINER" -q | xargs)
	if [[ -n $matchingStarted ]]; then
		docker stop $matchingStarted		
		echo "$NAME_CONTAINER$i container running then stopped"
	fi
	matching=$(docker ps -a --filter="name=$CONTAINER" -q | xargs)
	if [[ -n $matching ]]; then
		docker rm $matching		
		echo "$NAME_CONTAINER$i container stopped then remove it"
	fi
	if [[ $1 != *stop* ]];
	then
		runContainer $i
	fi

done



