#!/bin/bash

LOGFILE=~/log.txt
SUBNET_NODO2=192.168.133.0/24
SUBNET_NODO1=192.168.122.0/24
SCRIPT1_PATH="$(pwd)/docker-ubuntu20/launch.sh"
SCRIPT2_PATH="$(pwd)/setinternal-network.sh"
checkerror() {
	RESULT=$1
	if [ $RESULT != 0 ];then
		echo "[ERROR] Errors occured while launching, Check $LOGFILE"
		exit 127
	fi
}
helpFunction()
{
   echo ""
   echo "Usage: $0 -n nodeNumber -h IPExternalHost -s subnetContainers -i Interface"
   echo -e "\t-n Number of Node where this Script is launched (Esencial)"
   echo -e "\t-s Subnet where are allocated containers (ex:192.168.133.0/24) (Optional)"
   echo -e "\t-i Interface name of this machine (Optional)"
   echo -e "\t-h Ip external nodo (Host) (Optional)"
   echo -e "\n\t(By default Subnet Nodo1 $SUBNET_NODO1 and Subnet Nodo2 $SUBNET_NODO2)"
   exit 1
}
if [ $(id -u) -ne 0 ]; then
	echo "[WARN] Run this script as a Root user only" >&2
	exit 1
fi

#GETTING PARAMS
while getopts "n:s:i:h:" opt
do
   case "$opt" in
      n ) parameterN="$OPTARG" ;;
      s ) parameterS="$OPTARG" ;;
      i ) parameterI="$OPTARG" ;;
      h ) parameterExtHost="$OPTARG" ;;
      ? ) helpFunction ;; 
   esac
done

if [ -z "$parameterExtHost" ];then echo "[EXIT] It's esencial the IP of external Host";exit 127;fi
IPV4_ExtHOST=$parameterExtHost
#Check if ip of external nodo is available
if ping -c1 -w3 $IPV4_ExtHOST >/dev/null 2>&1
then
    echo "[Success] Ping responded; IP Node address allocated" >&2
else
    echo "[Warning] Ping did not respond; IP external Node address" >&2
fi

if [ -z "$parameterN" ]
then
   echo "Some of the esencial parameters are empty";
   helpFunction
else
   if  [ -z "$parameterI" ];then
      #Filter route to have host interface (get first match)
      INTERFACE_IPV4=$(route | grep '^default' | grep -o '[^ ]*$' |  head -n1)
      #Filter to have IP of specific interface
      IPV4_HOST=$(ip -4 addr show $INTERFACE_IPV4 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')

      #[CHANGED] check IPV4 host that have acces to intenet
      #Not work if we have two interfaces with internet access
      #wlp2s0 (WLAN)
      #enp4s0 (Ethernet)
      #IPV4_HOST=$(ip route get 8.8.8.8 | grep -oE '[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}' | tail -1)
      
   else 
      INTERFACE_IPV4=$parameterI
      #Filter to have IP of specific interface
      IPV4_HOST=$(ip -4 addr show $INTERFACE_IPV4 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')
   fi
	if [ -z "$parameterS" ];then
		SUBNET_NODO2=$parameterS
	fi
fi

if [ -z "$IPV4_HOST" ];then
   echo "Can not find out the ip";
   exit 1
fi

#
# By default: Subnet Nodo 1 192.168.122.0/24 and Sunbet Nodo 2 192.168.133.0/24
#
if [ $parameterN -gt 1 ];then host_int_subnetmask=$SUBNET_NODO2;else host_int_subnetmask=$SUBNET_NODO1;fi
#  
# Create Image docker and Launch two docker conntainer of the image
#

#CREATE A INTERNAL NETWORK BASED ON DOCKER CONTAINERS WITH NET BRIDGE
OUTPUT=$( source $SCRIPT2_PATH "$host_int_subnetmask")
checkerror $?

if [[  $RESULT_OUTPUT -eq 0 ]]; then
   echo -e '\n\t[Success] Internal Network'
   echo -e '\n\t _______ns_con1____                                                        _______ns_con2____'
   echo -e '\t (con1)[veth0_con1]-----[veth1_con1]{Bridge: con_br}[veth1_con2]------[veth0_con2](con2)\n'
fi
## Set the way between two internal networks on different hosts
if [ $parameterN -gt 1 ]
    then #NODO2
      echo -e "\t Node($parameterN)[$IPV4_HOST]  internal subnet: $SUBNET_NODO2"
      echo -e "\n\t Adding route on node 2 "
      ip route add $SUBNET_NODO1 via $IPV4_ExtHOST dev $INTERFACE_IPV4
    else #NODO1
      SUBNET_NODO2=192.168.133.0/24
      echo -e "\t Node($parameterN)[$IPV4_HOST] internal subnet: $SUBNET_NODO1"
      echo -e "\n\t Adding route on node 1 "
      
      ip route add $SUBNET_NODO2 via $IPV4_ExtHOST dev $INTERFACE_IPV4
fi
