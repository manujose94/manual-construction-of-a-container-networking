#!/bin/bash
# Readme
# This Script only work with subnet mask 24 (192.168.0.0/24)
#


LOGFILE=$(pwd)/log.txt
SCRIPT1_PATH="$(pwd)/docker-ubuntu20/launch.sh"
NAME_CONTAINER1=con1
NAME_CONTAINER2=con2
NS_CON1=ns_con1
NS_CON2=ns_con2
VETH0_CON1=veth0_cont1
VETH1_CON1=veth1_cont1
VETH0_CON2=veth0_cont2
VETH1_CON2=veth1_cont2

checkerror() {
	RESULT=$1
	if [ $RESULT != 0 ];then
		echo "[ERROR] Errors occured while launching, Check $LOGFILE"
        clear
		exit 127
	fi
}
clear(){
    unlink /var/run/netns/$NS_CON1 2> /dev/null
    unlink /var/run/netns/$NS_CON2 2> /dev/null
    ip link delete $VETH0_CON1 2> /dev/null
    ip link delete $VETH0_CON2 2> /dev/null
    ip link delete $VETH1_CON1 2> /dev/null
    ip link delete $VETH1_CON2 2> /dev/null
    ip link delete con_br type bridge 2> /dev/null
    rm -r /etc/netns/$NS_CON1/ 2> /dev/null
    rm -r /etc/netns/$NS_CON2/ 2> /dev/null
    OUTPUT=$( source $SCRIPT1_PATH stop)
    checkerror $?
}


#
#   MAIN
#

#Getting params
subnet=$1
subnetmask=${subnet%.*}
masksize=$(echo $subnet | cut -d '/' -f 2);
if [ -z "$subnetmask" ];then
   echo "Some of the esencial parameters are empty (number of subnet: default 192.168.122)">$LOGFILE 2>&1
   checkerror 1
fi
if [[ ! $masksize -eq 24 ]];then
echo "Only allow subnet with 24 size mask">$LOGFILE 2>&1
checkerror 1
fi


clear
echo "Creting docker containers (con1 and con2)......."
#Launch two image dockers with con1 and con2 names
OUTPUT=$( source $SCRIPT1_PATH) 
checkerror $?

echo "Setting up namespace runtime dir"
mkdir -p /var/run/netns

c1_pid="$(docker inspect --format '{{.State.Pid}}' $NAME_CONTAINER1)">> $LOGFILE 2>&1
checkerror $?
c2_pid="$(docker inspect --format '{{.State.Pid}}' $NAME_CONTAINER2)">> $LOGFILE 2>&1
checkerror $?

echo "Setting up container namespaces"
ln -s /proc/$c1_pid/ns/net /var/run/netns/$NS_CON1>> $LOGFILE 2>&1
checkerror $?
ln -s /proc/$c2_pid/ns/net /var/run/netns/$NS_CON2>> $LOGFILE 2>&1
checkerror $?
echo "Create and add veths to containers"
ip link add $VETH0_CON1 type veth peer name $VETH1_CON1 >> $LOGFILE 2>&1
checkerror $?
ip link add $VETH0_CON2 type veth peer name $VETH1_CON2 >> $LOGFILE 2>&1
checkerror $?
 
echo "Added each one of veth pair to each created namespace"
ip link set $VETH0_CON1 netns $NS_CON1
ip link set $VETH0_CON2 netns $NS_CON2

echo -e "\nCreate a linux [Bridge] default (con_br)"
ip link add name con_br type bridge
echo "Join [veth] container interfaces to [Bridge]"
ip link set $VETH1_CON1 master con_br
ip link set $VETH1_CON2 master con_br
echo "Assign IP to [Bridge]"
ip addr add $subnetmask.1/24 dev con_br>> $LOGFILE 2>&1
checkerror $?

echo -e "\nAssign IPs to veths"
ip netns exec $NS_CON1 ip addr add $subnetmask.11/24 dev $VETH0_CON1>> $LOGFILE 2>&1
checkerror $?
ip netns exec $NS_CON2 ip addr add $subnetmask.22/24 dev $VETH0_CON2>> $LOGFILE 2>&1
checkerror $?

echo "Up VETH0s device"
ip netns exec $NS_CON1 ip link set $VETH0_CON1 up
ip netns exec $NS_CON2 ip link set $VETH0_CON2 up

echo "Connect the Host Half to the [Bridge]"
ip link set $VETH1_CON1 up
ip link set $VETH1_CON2 up
echo "Up container [loopback]"
ip netns exec $NS_CON1 ip link set lo up
ip netns exec $NS_CON2 ip link set lo up
echo "Up [Bridge] device."
ip link set con_br up
echo -e "\nAdd bridge 'con_br' as default gateway for the container network"
ip netns exec $NS_CON1 ip route add default via $subnetmask.1 dev $VETH0_CON1
ip netns exec $NS_CON2 ip route add default via $subnetmask.1 dev $VETH0_CON2
echo "Allow ICMP traffic (Ping)"
iptables -A FORWARD -p icmp -j ACCEPT>> $LOGFILE 2>&1
checkerror $?
echo -e "\n Acces to Interne1 "
sysctl -w net.ipv4.ip_forward=1
#Enable sending requests and getting responses to/from internet
iptables -t nat -A POSTROUTING -s $subnetmask.0/24 ! -o con_br -j MASQUERADE>> $LOGFILE 2>&1
checkerror $?

#DNS
mkdir -p /etc/netns/$NS_CON1/
mkdir -p /etc/netns/$NS_CON2/
#DNS SERVER 8.8.8.8
echo "nameserver 8.8.8.8" > /etc/netns/$NS_CON/resolv.conf
echo "nameserver 8.8.8.8" > /etc/netns/$NS_CON/resolv.conf
#Allow packets that are not emitted to the host directly and not emmitted too.
iptables -P FORWARD ACCEPT

