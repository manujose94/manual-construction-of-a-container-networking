#!/bin/bash

# Program to set up two containers and connect them
# with a veth pair. basic point2point connectivity test

NAME_CONTAINER1=con1
NAME_CONTAINER2=con2
NS_CON1=ns_con1
NS_CON2=ns_con2
echo -e "\nTesting... "
echo -e "\n[1] Ping [container1] to [container2]\n"
ip netns exec $NS_CON1 ping -c 2 192.168.122.22
echo -e "\n[2] Ping [container2] to [container1]\n"
ip netns exec $NS_CON2 ping -c 2 192.168.122.11
echo -e "\n[3] Ping [container1] to [host]\n"
ip netns exec $NS_CON1 ping -c 2 192.168.122.1
echo -e "\n[4] Ping [host] to [container1]\n"
ping -c 2 192.168.122.1