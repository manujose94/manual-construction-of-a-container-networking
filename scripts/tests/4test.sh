NS_CON1=ns_con1
NS_CON2=ns_con2
check() {
	RESULT=$1
	if [ $RESULT != 0 ];then
		echo "[Problem] During during checkout"
		exit 127
    else echo -e "\t [Success]"
	fi
}
echo -e "\nTesting..."
echo -e "\n[1] Ping [container1] to [container2]\n"
ip netns exec $NS_CON1 ping -c 1 192.168.122.22 >/dev/null 2>&1
check $?
echo -e "\n[2] Ping [container2] to [container1]\n"
ip netns exec $NS_CON2 ping -c 1 192.168.122.11 >/dev/null 2>&1
check $?
echo -e "\n[3] Ping [container1] to [host]\n"
ip netns exec $NS_CON1 ping -c 1 192.168.122.1 >/dev/null 2>&1
check $?
echo -e "\n[4] Ping [host] to [container1]\n"
ping -c 2 192.168.122.1 >/dev/null 2>&1
check $?
echo -e "\n __External Nodes__ \n"
echo -e "\n[5] Ping [container1] to DNS Server [8.8.8.8](External host Internet)\n"
ip netns exec ns_con2 ping -c 2 8.8.8.8 >/dev/null 2>&1
check $?
echo -e "\n[6] Ping [container1] to external hostname [gitlab.com](Name resolution with DNS 8.8.8.8)\n"
ip netns exec ns_con1 ping -c 2 gitlab.com >/dev/null 2>&1
if [ $? != 0 ];then
	echo "\t[Problem] During during checkout"
	echo -e "\tMaybe the systemd-resolved service is failing"
	echo -e "\tCheck with: systemctl status systemd-resolved.service\n"
fi
echo -e "\n[7] Ping [container1] to external container on Nodo 2 [container1]\n"
ip netns exec ns_con1 ping -c 2 192.168.133.22>/dev/null 2>&1
check $?