#!/bin/bash

# Program to set up two containers and connect them
# with a veth pair. basic point2point connectivity test

NAME_CONTAINER1=con1
NAME_CONTAINER2=con2
NS_CON1=ns_con1
NS_CON2=ns_con2
VETH0_CON1=veth0_cont1
VETH1_CON1=veth1_cont1
VETH0_CON2=veth0_cont2
VETH1_CON2=veth1_cont2
SCRIPT1_PATH="$(pwd)/docker-ubuntu20/launch.sh"
checkerror() {
	RESULT=$1
	if [ $RESULT != 0 ];then
		echo "[ERROR] Errors occured while launching"
		exit 127
	fi
}
clear(){
    unlink /var/run/netns/$NS_CON1 2> /dev/null
    unlink /var/run/netns/$NS_CON2 2> /dev/null
    ip link delete $VETH0_CON1 2> /dev/null
    ip link delete $VETH0_CON2 2> /dev/null
    ip link delete $VETH1_CON1 2> /dev/null
    ip link delete $VETH1_CON2 2> /dev/null
    ip link delete con_br type bridge 2> /dev/null
    OUTPUT=$( source $SCRIPT1_PATH stop)
    checkerror $?
}
clear
echo "[1] Creting docker containers (con1 and con2)......."
#Launch two image dockers with con1 and con2 names
OUTPUT=$( source $SCRIPT1_PATH)
checkerror $?

echo "[2] Setting up namespace runtime dir"
mkdir -p /var/run/netns

c1_pid="$(docker inspect --format '{{.State.Pid}}' $NAME_CONTAINER1)"
c2_pid="$(docker inspect --format '{{.State.Pid}}' $NAME_CONTAINER2)"

echo "[3] Setting up container namespaces"
ln -s /proc/$c1_pid/ns/net /var/run/netns/$NS_CON1
ln -s /proc/$c2_pid/ns/net /var/run/netns/$NS_CON2

echo "[4] Create and add veths to the containers"
ip link add $VETH0_CON1 type veth peer name $VETH1_CON1
ip link add $VETH0_CON2 type veth peer name $VETH1_CON2
 
echo "[5] Added each one of veth pair to each created namespace"
ip link set $VETH0_CON1 netns $NS_CON1
ip link set $VETH0_CON2 netns $NS_CON2

echo -e "[6] \nCreate a linux [Bridge] default (con_br)"
ip link add name con_br type bridge
echo "[7] Join [veth] container interfaces to [Bridge]"
ip link set $VETH1_CON1 master con_br
ip link set $VETH1_CON2 master con_br
echo "[8] Assign IP to [Bridge]"
ip addr add 192.168.122.1/24 dev con_br

echo -e "[9] \nAssign IPs to veths"
ip netns exec $NS_CON1 ip addr add 192.168.122.11/24 dev $VETH0_CON1
ip netns exec $NS_CON2 ip addr add 192.168.122.22/24 dev $VETH0_CON2

echo "[10] Up VETH0s device"
ip netns exec $NS_CON1 ip link set $VETH0_CON1 up
ip netns exec $NS_CON2 ip link set $VETH0_CON2 up

echo "[11] Connect the Host Half to the [Bridge]"
ip link set $VETH1_CON1 up
ip link set $VETH1_CON2 up
echo "[12] Up container [loopback]"
ip netns exec $NS_CON1 ip link set lo up
ip netns exec $NS_CON2 ip link set lo up
echo "[13] Up [Bridge] device."
ip link set con_br up
echo -e "[14] \nAdd bridge 'con_br' as default gateway for the container network"
ip netns exec $NS_CON1 ip route add default via 192.168.122.1 dev $VETH0_CON1
ip netns exec $NS_CON2 ip route add default via 192.168.122.1 dev $VETH0_CON2
echo "[15] Allow ICMP traffic (Ping)"
iptables -A FORWARD -p icmp -j ACCEPT

# UTILS
# Confirm pingin checking arp entries
#ip netns exec ns_con2 arp -n

# Check Routes
#ip nets exec ns_con1 ip route

#Test set up listen on bridGe
#tcpdump -i con_br -n

