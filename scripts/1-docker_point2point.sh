#!/bin/bash

# Program to set up two  containers and connect them
# with a veth pair. basic point2point connectivity test

NAME_CONTAINER1=con1
NAME_CONTAINER2=con2
NS_CON1=ns_con1
NS_CON2=ns_con2
VETH_CON1=veth0_cont1
VETH_CON2=veth1_cont2
SCRIPT1_PATH="$(pwd)/docker-ubuntu20/launch.sh"
checkerror() {
	RESULT=$1
	if [ $RESULT != 0 ];then
		echo "[ERROR] Errors occured while launching"
		exit 127
	fi
}
clear(){
echo "Unlink namespaces"
unlink /var/run/netns/$NS_CON1 2> /dev/null
unlink /var/run/netns/$NS_CON2 2> /dev/null
OUTPUT=$( source $SCRIPT1_PATH stop)
checkerror $?
}

clear

#Launch two image dockers
echo "[1] Creting docker containers (con1 and con2)......."
OUTPUT=$( source $SCRIPT1_PATH)
checkerror $?
echo "[2] Setting up namespace runtime dir"
mkdir -p /var/run/netns

c1_pid="$(docker inspect --format '{{.State.Pid}}' $NAME_CONTAINER1)"
c2_pid="$(docker inspect --format '{{.State.Pid}}' $NAME_CONTAINER2)"

echo "[3] Setting up container namespaces"
ln -s /proc/$c1_pid/ns/net /var/run/netns/$NS_CON1
ln -s /proc/$c2_pid/ns/net /var/run/netns/$NS_CON2

echo "[4] Create and add veths to the containers"
ip link add $VETH_CON1 type veth peer name $VETH_CON2
echo "[5] Added one end of a veth pair to the namespace"
ip link set $VETH_CON1 netns $NS_CON1
ip link set $VETH_CON2 netns $NS_CON2

echo "[6] Assign IPs to veths (default subnet 192.168.122.0/24)"
ip netns exec $NS_CON1 ifconfig $VETH_CON1 192.168.122.1/24
ip netns exec $NS_CON2 ifconfig $VETH_CON2 192.168.122.2/24


#Probar testing
echo -e "\nTesting ping ($NAME_CONTAINER1)[$VETH_CON1]----->[$VETH_CON2]($NAME_CONTAINER2)"
docker exec $NAME_CONTAINER1 ping -c4 192.168.122.2

echo -e "\nTesting ping ($NAME_CONTAINER2)[$VETH_CON2]----->[$VETH_CON1]($NAME_CONTAINER1)"
docker exec $NAME_CONTAINER2 ping -c4 192.168.122.1
ip netns exec $NS_CON2 ping -c 2 192.168.122.1